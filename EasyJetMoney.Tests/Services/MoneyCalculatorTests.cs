using EasyJetMoney.Constants;
using EasyJetMoney.Interfaces;
using EasyJetMoney.Services;
using EasyJetMoney.Tests.Services.TestData;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EasyJetMoney.Tests.Services
{
	public class MoneyCalculatorTests
	{
		private readonly IMoneyCalculator _moneyCalculator;

		public MoneyCalculatorTests()
		{
			_moneyCalculator = new MoneyCalculator();
		}

		[Theory]
		[MemberData(nameof(MaxMoneyCalculatorTestData.GetSuccessData), MemberType = typeof(MaxMoneyCalculatorTestData))]
		public void Max_Should_Find_Largest_Amount_Of_Money(IEnumerable<IMoney> monies, IMoney expectedMoney)
		{
			var result = _moneyCalculator.Max(monies);

			result.Amount.Should().Be(expectedMoney.Amount);
		}

		[Theory]
		[MemberData(nameof(MaxMoneyCalculatorTestData.GetArgumentExceptionData), MemberType = typeof(MaxMoneyCalculatorTestData))]
		public void Max_Should_Throw_If_Currencies_Are_Different(IEnumerable<IMoney> monies)
		{
			Action act = () => _moneyCalculator.Max(monies);

			act.Should().Throw<ArgumentException>()
					.WithMessage("All monies are not in the same currency.");
		}

		[Theory]
		[MemberData(nameof(SumPerCurrencyMoneyCalculatorTestData.GetSuccessData), MemberType = typeof(SumPerCurrencyMoneyCalculatorTestData))]
		public void SumPerCurrency_Should_Calculate_Sum_Per_Currency(IEnumerable<IMoney> monies, IEnumerable<IMoney> expectedMonies)
		{
			var sumPerCurrency = _moneyCalculator.SumPerCurrency(monies);

			foreach (var expectedMoney in expectedMonies)
			{
				var result = sumPerCurrency.FirstOrDefault(x => x.Currency == expectedMoney.Currency);
				result.Amount.Should().Be(expectedMoney.Amount);
			}
		}
	};
}