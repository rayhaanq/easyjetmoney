﻿using EasyJetMoney.Constants;
using EasyJetMoney.Interfaces;
using EasyJetMoney.Models;
using System.Collections.Generic;
using Xunit;

namespace EasyJetMoney.Tests.Services.TestData
{
	public class SumPerCurrencyMoneyCalculatorTestData
	{
		public static TheoryData<IEnumerable<IMoney>, IEnumerable<IMoney>> GetSuccessData =>
			new TheoryData<IEnumerable<IMoney>, IEnumerable<IMoney>>
		{
				{
					new List<IMoney>
					{
						new Money(10, Currency.GBP),
						new Money(20, Currency.GBP),
						new Money(50, Currency.GBP),
					},
					new List<IMoney>
					{
						new Money(80, Currency.GBP),
					}
				},
				{
					new List<IMoney>
					{
						new Money(10, Currency.GBP),
						new Money(20, Currency.GBP),
						new Money(50, Currency.EUR),
					},
					new List<IMoney>
					{
						new Money(30, Currency.GBP),
						new Money(50, Currency.EUR),
					}
				},
				{
					new List<IMoney>
					{
						new Money(10, Currency.GBP),
						new Money(20, Currency.USD),
						new Money(50, Currency.EUR),
					},
					new List<IMoney>
					{
						new Money(10, Currency.GBP),
						new Money(20, Currency.USD),
						new Money(50, Currency.EUR),
					}
				},
				{
					new List<IMoney>
					{
						new Money(80, Currency.USD),
						new Money(30, Currency.GBP),
						new Money(20, Currency.USD),
						new Money(10, Currency.EUR),
						new Money(5, Currency.USD),
						new Money(30, Currency.EUR),
						new Money(25, Currency.GBP),
					},
					new List<IMoney>
					{
						new Money(105, Currency.USD),
						new Money(55, Currency.GBP),
						new Money(40, Currency.EUR),
					}
				}
		};
	}
}