﻿using EasyJetMoney.Constants;
using EasyJetMoney.Interfaces;
using EasyJetMoney.Models;
using System.Collections.Generic;
using Xunit;

namespace EasyJetMoney.Tests.Services.TestData
{
	public class MaxMoneyCalculatorTestData
	{
		public static TheoryData<IEnumerable<IMoney>, IMoney> GetSuccessData =>
		 new TheoryData<IEnumerable<IMoney>, IMoney>
		 {
			 {
					new List<IMoney>
					{
						new Money(10, Currency.GBP),
						new Money(20, Currency.GBP),
						new Money(30, Currency.GBP),
					},
					new Money(30, Currency.GBP)
			 },
			 {
					new List<IMoney>
					{
						new Money(20, Currency.EUR),
						new Money(50, Currency.EUR),
						new Money(10, Currency.EUR),
					},
					new Money(50, Currency.EUR)
			 },
			 {
					new List<IMoney>
					{
						new Money(80, Currency.USD),
						new Money(50, Currency.USD),
						new Money(30, Currency.USD),
					},
						new Money(80, Currency.USD)
				},
			};

		public static TheoryData<IEnumerable<IMoney>> GetArgumentExceptionData =>
		new TheoryData<IEnumerable<IMoney>>
		{
			{
				new List<IMoney>
				{
					new Money(10, Currency.GBP),
					new Money(20, Currency.EUR),
					new Money(30, Currency.GBP),
				}
			},
			{
				new List<IMoney>
				{
					new Money(20, Currency.EUR),
					new Money(50, Currency.USD),
					new Money(10, Currency.EUR),
				}
			},
			{
			new List<IMoney>
				{
					new Money(80, Currency.USD),
					new Money(50, Currency.GBP),
					new Money(30, Currency.USD),
				}
			},
		};
	}
}