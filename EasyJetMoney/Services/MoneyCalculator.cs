﻿using EasyJetMoney.Interfaces;
using EasyJetMoney.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EasyJetMoney.Services
{
	public class MoneyCalculator : IMoneyCalculator
	{
		public IMoney Max(IEnumerable<IMoney> monies)
		{
			var numberOfCurrencies = monies.GroupBy(x => x.Currency).Count();
			if (numberOfCurrencies > 1)
			{
				throw new ArgumentException("All monies are not in the same currency.");
			}

			var maxVal = monies.Max(x => x.Amount);
			return monies.FirstOrDefault(x => x.Amount == maxVal);
		}

		public IEnumerable<IMoney> SumPerCurrency(IEnumerable<IMoney> monies)
		{
			return monies.GroupBy(x => x.Currency)
				.Select(x => new Money(x.Sum(x => x.Amount), x.FirstOrDefault().Currency));
		}
	}
}