﻿using EasyJetMoney.Interfaces;

namespace EasyJetMoney.Models
{
	public class Money : IMoney
	{
		public decimal Amount { get; }

		public string Currency { get; }

		public Money(decimal amount, string currency)
		{
			Amount = amount;
			Currency = currency;
		}
	}
}