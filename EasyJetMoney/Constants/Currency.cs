﻿namespace EasyJetMoney.Constants
{
	public class Currency
	{
		public const string GBP = "GBP";
		public const string EUR = "EUR";
		public const string USD = "USD";
	}
}